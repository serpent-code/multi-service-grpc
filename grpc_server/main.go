// Package main implements a server for Greeter service.
package main

import (
	"context"
	"database/sql"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"os"

	_ "github.com/glebarez/go-sqlite"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc"
	pb "grpc_server/proto"
)

var (
	port = flag.Int("port", 50051, "The server port")
)

type authenticator_server struct {
	pb.UnimplementedAuthenticatorServer
}

type change_password_server struct {
	pb.UnimplementedPasswordChangerServer
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func (s *authenticator_server) Auth(ctx context.Context, in *pb.AuthRequest) (*pb.AuthReply, error) {
	log.Printf("Received: username: %v, password: %v", in.GetUsername(), in.GetPassword())

	db, err := sql.Open("sqlite", "./grpc_server_sqlite.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	stmt, err := db.Prepare("select username, password, is_active from users where username = ?")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	var username string
	var password string
	var is_active int
	err = stmt.QueryRow(in.GetUsername()).Scan(&username, &password, &is_active)
	if err != nil {
		log.Println(err)
		return &pb.AuthReply{Authenticated: false}, nil
	}
	if is_active == 0 {
		return &pb.AuthReply{Authenticated: false}, nil
	}

	// THis might be a demo app, but still passwords are salted with username!
	if CheckPasswordHash(in.GetUsername()+in.GetPassword(), password) {
		return &pb.AuthReply{Authenticated: true}, nil
	}

	return &pb.AuthReply{Authenticated: false}, nil
}

func (s *change_password_server) ChangePassword(ctx context.Context, in *pb.PasswordChangeRequest) (*pb.PasswordChangeReply, error) {
	log.Printf("Received: username: %v, password: %v", in.GetUsername(), in.GetPassword())

	db, err := sql.Open("sqlite", "./grpc_server_sqlite.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("UPDATE users SET password = $1 WHERE username = $2")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	salted_password := in.GetUsername() + in.GetPassword()
	hashed_password, _ := HashPassword(salted_password)
	_, err = stmt.Exec(hashed_password, in.GetUsername())
	if err != nil {
		log.Println(err)
		return &pb.PasswordChangeReply{Success: false}, nil
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
		return &pb.PasswordChangeReply{Success: false}, nil
	}

	return &pb.PasswordChangeReply{Success: true}, nil
}

func main() {

	if _, err := os.Stat("./grpc_server_sqlite.db"); errors.Is(err, os.ErrNotExist) {
		panic("grpc_server_sqlite.db was not found")
	}

	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterAuthenticatorServer(s, &authenticator_server{})
	pb.RegisterPasswordChangerServer(s, &change_password_server{})

	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
