#!/bin/sh

sqlite3 grpc_server_sqlite.db <<EOF
create table users (id INTEGER PRIMARY KEY, username TEXT, password TEXT, is_active INTEGER default 1);
insert into users (username, password) values ('ahmad','\$2a\$10\$YkCHAuNgx7/2DB7Z0YEtb.8JCuKUPURZSmQnSJMkONdu9vzFbY7ii');
insert into users (username, password) values ('akbar','\$2a\$10\$ZLnlRY.QR0KiUcUabS5DtOCdZIfx2NX9gcsICjnpAHGgBnnxTx5bO');
insert into users (username, password) values ('rebecca','\$2a\$10\$/ydPpzzBwmCQdNOTMhvIT.Oj2fcMj1m6InBa6kwGlSzERN46oMDQi');
insert into users (username, password) values ('joe','\$2a\$10\$q/EQsLGcbptoFaZWdy6pi.fnqbfrCgiDbFfD3NCz1YI9UR6D4AOxy');
insert into users (username, password) values ('dude','\$2a\$10\$N5cnfxPcjFEP2Kg4JIBz8exPo2203GdhlL91ehkBNCwavd.MHqDMC');
select * from users;
EOF
