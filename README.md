# Multi Service gRPC

An example project with an http server that calls gRPC functions in another service which does authentication from an SQLite database


## Getting started

### Seeding the database

The `grpc_server_sqlite.db` file has to exist in grpc_server directory.

To seed the database and make this file, run the `database_seeder.sh` file in grpc_server directory.

For convenience, this sqlite database file is also commited to git.

users in the database are as following:

```
username | password     | is_active
-------------------------------
ahmad    | 1234         | 1
akbar    | simple       | 1
rebecca  | ok           | 1
joe      | fernando     | 1
dude     | coolstorybro | 1
```

### Endpoints

http server has the following routes:

`/auth/login`

takes a JSON body with username and password, return JWT token


`/auth/change-password`

takes a JSON body with password, and JWT token in Authorization header. Changes the password for the user which JWT is provided.





### Compiling gRPC code on changes

Copy the changed `app.proto` file into both `http_server/proto` and `grpc_server/proto` directories. Then:

```
cd http_server
protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative proto/app.proto

cd grpc_server
protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative proto/app.proto

```

