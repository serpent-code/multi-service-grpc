package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	pb "http_server/proto"
)

var (
	addr = flag.String("addr", "localhost:50051", "the address to connect to")
)

func setupRouter() *gin.Engine {
	r := gin.Default()

	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	r.POST("/auth/login", func(c *gin.Context) {

		var input_json struct {
			Username string `json:"username" binding:"required"`
			Password string `json:"password" binding:"required"`
		}

		err := c.BindJSON(&input_json)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"status": "failed", "reason": "username and password are required"})
			return
		}

		conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "failed", "reason": "could not connect to gRPC server"})
			log.Printf("did not connect to gRPC server: %v", err)
			return
		}
		defer conn.Close()
		gc := pb.NewAuthenticatorClient(conn)

		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		gr, err := gc.Auth(ctx, &pb.AuthRequest{Username: input_json.Username, Password: input_json.Password})
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "failed", "reason": "could not call Auth gRPC function"})
			log.Printf("could not call Auth gRPC function: %v", err)
			return
		}

		if gr.GetAuthenticated() {
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"username": input_json.Username,
			})

			jwt_key := []byte("MySuperSecretJWTsecret")
			tokenString, err := token.SignedString(jwt_key)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"status": "failed", "reason": "could not make JWT token"})
				log.Printf("could not make JWT token: %v", err)
				return
			}
			c.JSON(http.StatusOK, gin.H{"status": "success", "token": tokenString})
		} else {
			c.JSON(http.StatusUnauthorized, gin.H{"status": "failed", "reason": "username and password are incorrect"})
		}

	})

	r.POST("/auth/change-password", func(c *gin.Context) {

		auth_header := c.GetHeader("Authorization")
		log.Println(auth_header)
		auth_header = strings.Replace(auth_header, "Bearer ", "", 1)
		log.Println(auth_header)

		if len(auth_header) == 0 {
			c.JSON(http.StatusUnauthorized, gin.H{"status": "failed", "reason": "Authorization header is required"})
			return
		}

		jwt_key := []byte("MySuperSecretJWTsecret")
		claims := jwt.MapClaims{}
		_, err := jwt.ParseWithClaims(auth_header, claims, func(token *jwt.Token) (interface{}, error) {
			return jwt_key, nil
		})
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"status": "failed", "reason": "failed to parse JWT token"})
			log.Printf("failed to parse JWT token: %v", err)
			return
		}
		jwt_username := claims["username"].(string)

		var input_json struct {
			Password string `json:"password" binding:"required"`
		}

		err = c.BindJSON(&input_json)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"status": "failed", "reason": "password is required"})
			return
		}

		conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "failed", "reason": "could not connect to gRPC server"})
			log.Printf("did not connect to gRPC server: %v", err)
			return
		}
		defer conn.Close()
		gc := pb.NewPasswordChangerClient(conn)

		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		gr, err := gc.ChangePassword(ctx, &pb.PasswordChangeRequest{Username: jwt_username, Password: input_json.Password})
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "failed", "reason": "could not call ChangePassword gRPC function"})
			log.Printf("could not call ChangePassword gRPC function: %v", err)
			return
		}

		if gr.GetSuccess() {
			c.JSON(http.StatusOK, gin.H{"status": "success", "reason": "password was updated successfully"})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "failed", "reason": "password update failed"})
		}

	})

	return r
}

func main() {
	flag.Parse()
	r := setupRouter()

	r.Run(":8080")
}
